import 'dart:convert';

import 'package:newsroom/services/webservice.dart';
import 'package:newsroom/utils/constants.dart';

class NewsArticle {
  final String title;
  final String description;
  final String content;
  final String urlToImage;
  final String publishedAt;

  NewsArticle(
      {this.title,
      this.description,
      this.content,
      this.urlToImage,
      this.publishedAt});

  factory NewsArticle.fromJson(Map<String, dynamic> json) {
    return NewsArticle(
        title: json["title"],
        description: json["description"],
        content: json["content"],
        urlToImage: json["urlToImage"],
        publishedAt: json["publishedAt"]);
  }

  static Resource<List<NewsArticle>> get all {
    return Resource(
        url: Constants.HEADLINE_NEWS_URL,
        parse: (response) {
          final result = json.decode(response.body);
          Iterable list = result["articles"];
          return list.map((model) => NewsArticle.fromJson(model)).toList();
        });
  }
}
