import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:newsroom/models/newsarticle.dart';

class NewsDetailsPage extends StatefulWidget {
  final NewsArticle newsArticle;

  const NewsDetailsPage({Key key, this.newsArticle}) : super(key: key);

  @override
  _NewsDetailsPageState createState() => _NewsDetailsPageState();
}

class _NewsDetailsPageState extends State<NewsDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Text(
              widget.newsArticle.title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            Divider(),
            Text(widget.newsArticle.content),
          ],
        ),
      ),
    );
  }
}
