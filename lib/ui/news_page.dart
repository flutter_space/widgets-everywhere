import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:newsroom/models/newsarticle.dart';
import 'package:newsroom/services/webservice.dart';
import 'package:newsroom/ui/news_detail_page.dart';
import 'package:newsroom/utils/constants.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  List<NewsArticle> _newsArticles = List<NewsArticle>();

  @override
  void initState() {
    super.initState();
    _populateNewsArticle();
  }

  void _populateNewsArticle() {
    WebService().load(NewsArticle.all).then((newsArticles) {
      setState(() {
        _newsArticles = newsArticles;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("News Room"),
        ),
        body: SafeArea(
          child: _buildListView(),
        ));
  }

  Widget _buildListView() {
    if (_newsArticles.isEmpty) {
      return Center(child: CircularProgressIndicator());
    } else {
      return ListView.builder(
        itemCount: _newsArticles.length,
        itemBuilder: _buildCardItemForListView,
      );
    }
  }

  Widget _buildSimpleListItem(BuildContext context, int index) {
    var article = _newsArticles[index];
    return Container(
        child: Column(
      children: <Widget>[Text(article.title), Divider()],
    ));
  }

  ListTile _buildListTileItemForListView(BuildContext context, int index) {
    var article = _newsArticles[index];
    return ListTile(
      contentPadding: EdgeInsets.all(8),
      leading: article.urlToImage == null
          ? Image.asset(Constants.NEWS_PLACEHOLDER_IMAGE)
          : Image.network(article.urlToImage),
      title: Text(article.title),
      trailing: Icon(Icons.arrow_right),
      onTap: () => _showNewsDetails(index),
    );
  }

  Card _buildCardItemForListView(BuildContext context, int index) {
    var article = _newsArticles[index];
    return Card(
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: InkWell(
        onTap: () => _showNewsDetails(index),
        child: Column(
          children: <Widget>[
            article.urlToImage == null
                ? Image.asset(Constants.NEWS_PLACEHOLDER_IMAGE)
                : Image.network(article.urlToImage),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                article.title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                article.description,
                style: TextStyle(fontSize: 11),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showNewsDetails(int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              NewsDetailsPage(newsArticle: _newsArticles[index]),
        ));
  }
}
