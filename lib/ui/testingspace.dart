import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TestingSpace extends StatefulWidget {
  @override
  _TestingSpaceState createState() => _TestingSpaceState();
}

class _TestingSpaceState extends State<TestingSpace> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Center(
        child: Container(
          child: Text("This is clickable"),
        ),
      ),
      onTap: DoSomething,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter"),
      ),
      body: Center(
        child: Container(
          child: (Text("Hello SoCraOs")),
        ),
      ),
    );
  }
}

DoSomething() {}
