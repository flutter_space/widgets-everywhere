class Constants {
  //TODO add api key from newsapi.org
  static const String HEADLINE_NEWS_URL =
      'https://newsapi.org/v2/top-headlines?country=de&apiKey=insertYourAPIKeyHere';

  static const String NEWS_PLACEHOLDER_IMAGE = 'assets/news-placeholder.png';
}
